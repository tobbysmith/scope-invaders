
```
#!text

███████╗ ██████╗ ██████╗ ██████╗ ███████╗    ██╗███╗   ██╗██╗   ██╗ █████╗ ██████╗ ███████╗██████╗ ███████╗
██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔════╝    ██║████╗  ██║██║   ██║██╔══██╗██╔══██╗██╔════╝██╔══██╗██╔════╝
███████╗██║     ██║   ██║██████╔╝█████╗      ██║██╔██╗ ██║██║   ██║███████║██║  ██║█████╗  ██████╔╝███████╗
╚════██║██║     ██║   ██║██╔═══╝ ██╔══╝      ██║██║╚██╗██║╚██╗ ██╔╝██╔══██║██║  ██║██╔══╝  ██╔══██╗╚════██║
███████║╚██████╗╚██████╔╝██║     ███████╗    ██║██║ ╚████║ ╚████╔╝ ██║  ██║██████╔╝███████╗██║  ██║███████║
╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚══════╝    ╚═╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝  ╚═╝╚═════╝ ╚══════╝╚═╝  ╚═╝╚══════╝
```

                                                                                                           
### The excellent space invaders like game for Hook ###

Defend the Galaxy from scope creep by shooting all the JB's while avoiding the spreadsheets.

### Install ###
* Clone the repo
* run bower update
* enjoy!

### How to Play ###

* Right / Left arrows or AD to move the ship left and right
* Space Bar to fire

### Options ###
* Set var jbMode = false; for regular play
* Set var nosound = true; for no background music

### Contribution ###
Special thanks to Brad Hicks for the initial idea

Thanks to JB quote generator for all the fun quotes

Thanks to Eric Brackmann for some creative direction

#### Beta Testers ####
* Umer Arif
* Michael Bianculli
* Mason Bailey
* Dhaval Damniwala
* Brad Hicks