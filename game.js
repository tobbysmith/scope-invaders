	// Load Audio
	var backgroundMusic = new Audio('sound/defense_line.mp3');
	backgroundMusic.addEventListener('ended', function() {
		this.currentTime = 0;
		this.play();
	});
	var laserSound = new Audio('sound/shoot.wav');

	// Detect JB Mode
	try {
		if(typeof jbMode === 'undefined') {
			jbMode = true;
		}
	} catch(e) {
		jbMode = false;
	}

	// Create the canvas
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width = 800;
	canvas.height = 600;

	document.body.appendChild(canvas);
	document.getElementById('start-button').addEventListener('click', function() {
		init();
	});

	// The main loop
	var lastTime;
	function main() {
		var now = Date.now();
		var dt = (now - lastTime) / 1000.0;

		update(dt);
		render();

		lastTime = now;
		requestAnimationFrame(main);
	}


	function init() {
		document.getElementById('start-screen').style.display = 'none';
		document.getElementById('play-again-success').addEventListener('click', function() {
			reset();
		});
		document.getElementById('play-again-fail').addEventListener('click', function() {
			reset();
		});

		ctx.drawImage(resources.get('images/starsbackground.jpg'), 0, 0, 800, 600);

		reset();

		lastTime = Date.now();

		main();
	}


	function startGameReady() {
		makeInvaders();
		try {
			if(!nosound) {
				backgroundMusic.play();
			}
		} catch (e) {
			backgroundMusic.play();
		}

		// Set images
		if(!jbMode) {
			var endGameImages = document.querySelectorAll(".endgame-image");



			for (var i = 0; i < endGameImages.length; i++) {
				endGameImages[i].src = 'images/invader_endgame.jpg';
			}
		}

		// Show play button
		document.getElementById('start-button').style.display = 'block';
	}

	function makeInvaders() {
		// Need 3 rows of 8
		for(var i=0; i<24; i++) {
			var x = 60 + (i % 8) * 60;
			var y = 60 + (i % 3) * 60;

			if(jbMode) {
				enemies.push({
					pos: [x, y],
					sprite: new Sprite('images/invader_jb.png', [0, 0], [32, 32])
				});
			} else {
				enemies.push({
					pos: [x, y],
					sprite: new Sprite('images/invader32x32x4.png', [0, 0], [32, 32], 16, [0, 1, 2, 3])
				});
			}
		}
	}

	// Load resources
	if(jbMode) {
		resources.load([
			'images/sprites.png',
			'images/invader_jb.png',
			'images/bullet.png',
			'images/enemy-bullet.png',
			'images/player.png',
			'images/starsbackground.jpg'
		]);
	} else {
		resources.load([
			'images/sprites.png',
			'images/invader32x32x4.png',
			'images/bullet.png',
			'images/bullet-enemy.png',
			'images/player.png',
			'images/starsbackground.jpg'
		]);
	}


	// On ready, initialize game
	resources.onReady(startGameReady);

	// Game State
	var player = {
		pos: [canvas.width / 2, 400],
		sprite: new Sprite('images/player.png', [0, 0], [28, 21])
	};

	// Entities
	var bullets = [];
	var enemyBullets = [];
	var enemies = [];
	var explosions = [];

	// Misc
	var lastFire = Date.now();
	var gameTime = 0;
	var isPlayerHit = false;
	var isGameOver;
	var isGameOverMsg = false;


	// Score
	var score = 0;
	var scoreEl = document.getElementById('score-counter');

	// Speed in pixels per second
	var playerSpeed = 200;
	var bulletSpeed = 500;
	var enemyBulletSpeed = 200;
	var enemySpeed = 100;
	var playerFireRate = 300;
	var enemyFireRate = 0.997;

	// Update game objects
	function update(dt) {
		gameTime += dt;

		for(var i=0; i<enemies.length; i++) {
			if (Math.random() > enemyFireRate && !invadersBelow(enemies[i])) {
				var x = enemies[i].pos[0] + enemies[i].sprite.size[0] / 2;
				var y = enemies[i].pos[1] + enemies[i].sprite.size[1] / 2;

				// TODO: Math.random() - 0.5 for X? Test this
				if(jbMode) {
					enemyBullets.push({
						pos: [x + 4, y],
						dir: 'down',
						sprite: new Sprite('images/enemy-bullet.png', [0, 0], [16, 16])
					});
				} else {
					enemyBullets.push({
						pos: [x + 4, y],
						dir: 'down',
						sprite: new Sprite('images/bullet-enemy.png', [0, 0], [16, 16])
					});
				}
			}
		}

		handleInput(dt);
		updateEntities(dt);
		checkCollisions();

		scoreEl.innerHTML = score;
	}

	// Are there invaders below?
	function invadersBelow(invader) {
		return enemies.filter(function (b) {
				return b.pos[1] > invader.pos[1] &&
					b.pos[0] - invader.pos[0] < invader.pos[0];
			}).length > 0;
	}

	// Handle keyboard input
	function handleInput(dt) {
		if(input.isDown('LEFT') || input.isDown('a')) {
			player.pos[0] -= playerSpeed * dt;
		}

		if(input.isDown('RIGHT') || input.isDown('d')) {
			player.pos[0] += playerSpeed * dt;
		}

		if(input.isDown('SPACE') &&
			!isGameOver &&
			Date.now() - lastFire > playerFireRate) {
			var x = player.pos[0] + player.sprite.size[0] / 2;
			var y = player.pos[1] + player.sprite.size[1] / 2;

			bullets.push({ pos: [x - 4, y - 30],
				dir: 'up',
				sprite: new Sprite('images/bullet.png', [0, 0], [6, 36])
			});

			if(laserSound.currentTime > 0) {
				laserSound.currentTime = 0;
			}

			laserSound.play();

			lastFire = Date.now();
		}
	}

	function updateEntities(dt) {
		// Update the player sprite animation
		player.sprite.update(dt);

		// Update all the bullets
		for(var i=0; i<bullets.length; i++) {
			var bullet = bullets[i];

			switch(bullet.dir) {
				case 'up': bullet.pos[1] -= bulletSpeed * dt; break;
				case 'down': bullet.pos[1] += bulletSpeed * dt; break;
				default:
					bullet.pos[0] += bulletSpeed * dt;
			}

			// Remove the bullet if it goes offscreen
			if(bullet.pos[1] < 0 || bullet.pos[1] > canvas.height ||
				bullet.pos[0] > canvas.width) {
				bullets.splice(i, 1);
				i--;
			}
		}

		// Update all enemy bullets
		for(var i=0; i<enemyBullets.length; i++) {
			var enemyBullet= enemyBullets[i];
			switch(enemyBullet.dir) {
				case 'up': enemyBulletBullet.pos[1] -= enemyBulletSpeed * dt; break;
				case 'down': enemyBullet.pos[1] += enemyBulletSpeed * dt; break;
				default:
					enemyBullet.pos[0] += enemyBulletSpeed * dt;
			}

			// Remove the bullet if it goes offscreen
			if(enemyBullet.pos[1] < 0 || enemyBullet.pos[1] > canvas.height ||
				enemyBullet.pos[0] > canvas.width) {
				enemyBullets.splice(i, 1);
				i--;
			}
		}

		// Check invader group direction
		for(var i=0; i<enemies.length; i++) {
			if (enemies[i].pos[0] < 0 || enemies[i].pos[0] + enemies[i].sprite.size[0] > canvas.width) {
				// Reverse travel direction
				enemySpeed = -enemySpeed;
				break;
			}
		}

		// Update all the enemies
		for(var i=0; i<enemies.length; i++) {

			enemies[i].pos[0] += enemySpeed * dt;
			enemies[i].sprite.update(dt);

			// Remove if offscreen
			if(enemies[i].pos[0] + enemies[i].sprite.size[0] < 0) {
				enemies.splice(i, 1);
				i--;
			}
		}

		// Update all the explosions
		for(var i=0; i<explosions.length; i++) {
			explosions[i].sprite.update(dt);

			// Remove if animation is done
			if(explosions[i].sprite.done) {
				explosions.splice(i, 1);
				i--;
			}
		}

		// Check if there still are enemies
		if(enemies.length === 0) {
			gameOver('success');
		}
	}

	// Collision Detection
	function collides(x, y, r, b, x2, y2, r2, b2) {
		return !(r <= x2 || x > r2 ||
		b <= y2 || y > b2);
	}

	function boxCollides(pos, size, pos2, size2) {
		return collides(pos[0], pos[1],
			pos[0] + size[0], pos[1] + size[1],
			pos2[0], pos2[1],
			pos2[0] + size2[0], pos2[1] + size2[1]);
	}

	function checkCollisions() {
		checkPlayerBounds();

		// Run collision detection for player and enemy bullets
		var pos = player.pos;
		var size = player.sprite.size;

		for(var i=0; i<enemyBullets.length; i++) {
			var pos2 = enemyBullets[i].pos;
			var size2 = enemyBullets[i].sprite.size;

			if(boxCollides(pos, size, pos2, size2)) {
				// Add an explosion
				explosions.push({
					pos: pos,
					sprite: new Sprite('images/sprites.png',
						[0, 117],
						[39, 39],
						16,
						[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
						null,
						true)
				});

				// Remove the bullet and stop this iteration
				enemyBullets.splice(i, 1);
				isPlayerHit = true;
				break;
			}
		}

		// Run collision detection for all enemies and bullets
		for(var i=0; i<enemies.length; i++) {
			var pos = enemies[i].pos;
			var size = enemies[i].sprite.size;

			for(var j=0; j<bullets.length; j++) {
				var pos2 = bullets[j].pos;
				var size2 = bullets[j].sprite.size;

				if(boxCollides(pos, size, pos2, size2)) {
					// Remove the enemy
					enemies.splice(i, 1);
					i--;

					// Add score
					score += 100;

					// Add an explosion
					explosions.push({
						pos: pos,
						sprite: new Sprite('images/sprites.png',
							[0, 117],
							[39, 39],
							16,
							[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
							null,
							true)
					});

					// Remove the bullet and stop this iteration
					bullets.splice(j, 1);
					break;
				}
			}

			if(boxCollides(pos, size, player.pos, player.sprite.size) || isPlayerHit) {
				gameOver('fail');
			}
		}
	}

	function checkPlayerBounds() {
		// Check bounds
		if(player.pos[0] < 0) {
			player.pos[0] = 0;
		}
		else if(player.pos[0] > canvas.width - player.sprite.size[0]) {
			player.pos[0] = canvas.width - player.sprite.size[0];
		}

		if(player.pos[1] < 0) {
			player.pos[1] = 0;
		}
		else if(player.pos[1] > canvas.height - player.sprite.size[1]) {
			player.pos[1] = canvas.height - player.sprite.size[1];
		}
	}



	// Draw everything
	function render() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.drawImage(resources.get('images/starsbackground.jpg'), 0, 0, 800, 600);

		// Render the player if the game isn't over
		if(!isGameOver) {
			renderEntity(player);
		}

		renderEntities(bullets);
		renderEntities(enemies);
		renderEntities(enemyBullets);
		renderEntities(explosions);
	}

	function renderEntities(list) {
		for(var i=0; i<list.length; i++) {
			renderEntity(list[i]);
		}
	}

	function renderEntity(entity) {
		ctx.save();
		ctx.translate(entity.pos[0], entity.pos[1]);
		entity.sprite.render(ctx);
		ctx.restore();
	}

	// Game over
	function gameOver(state) {

		document.getElementById('game-over-overlay').style.display = 'block';

		if(!isGameOverMsg) {
			if (state == 'success') {
				if(jbMode) {
					showText('jb-message-success', Jb.getRandomQuote('success'), 100);
				} else {
					showText('jb-message-success', 'The universe is a better place thanks to you!', 100);
				}

				document.getElementById('game-over-success').style.display = 'block';
			} else {
				if(jbMode) {
					showText('jb-message-fail', Jb.getRandomQuote('fail'), 100);
				} else {
					showText('jb-message-fail', 'Better Luck Next Time!', 100);
				}

				document.getElementById('game-over-fail').style.display = 'block';
			}
		}

		isGameOverMsg = true;
		isGameOver = true;
	}

	function showText(destination, message, speed) {
		document.getElementById(destination).innerHTML = '';
		
		var i = 0;
		var interval = setInterval(function(){
			document.getElementById(destination).innerHTML += message.charAt(i);
			i++;
			if (i > message.length){
				clearInterval(interval);
			}
		}, speed);
	}

	// Reset game to original state
	function reset() {
		document.getElementById('game-over-success').style.display = 'none';
		document.getElementById('game-over-fail').style.display = 'none';
		document.getElementById('game-over-overlay').style.display = 'none';
		isGameOver = false;
		isGameOverMsg = false;
		isPlayerHit = false;
		gameTime = 0;
		score = 0;

		player.pos = [canvas.width / 2, 500];

		enemies = [];
		enemyBullets = [];
		bullets = [];

		makeInvaders();
	}