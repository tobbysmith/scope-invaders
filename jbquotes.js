var Jb = (function() {
	var jbWinQuotes = [
		'Uhh.... Sounds Good',
		'That\'s how it is, right? ​*shrug*',
		'Cool...right?  That\'s how it is, right? ​*shrug*',
		'Uhhhhh...ok, I will send you an updated version.',
		'Sounds good!...uhh...​*mumbled speech..*​ Sounds good!',
		'uhhhhh...good to know.',
		'uhhhhh...I love it.',
		'uhhhhh...thanks dude.'
	];

	var jbFailQuotes = [
		'uhhhhh...Not convinced.',
		'No way dude.',
		'I\'m not feeling it.',
		'I\'m not buying it.',
		'I\'m not buying it...right?  Not convinced.',
		'Sounds  good...right?  Ha. Ha. No way.',
		'Ha. Ha. No way.'
	];

	function getRandomQuote(action) {
		switch (action) {
			case 'success':
				return jbWinQuotes[Math.floor(Math.random() * jbWinQuotes.length)];
				break;
			case 'fail':
				return jbFailQuotes[Math.floor(Math.random() * jbFailQuotes.length)];
				break;
		}
	}

	return {
		getRandomQuote : getRandomQuote
	}
})();