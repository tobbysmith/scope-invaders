describe("Scope Invaders Game", function() {
	describe("Player", function() {
		it('Should have a player', function () {
			expect(player).toBeDefined();
		});

		it('Player should have a sprite and it should be correct', function () {
			expect(player.sprite).toBeDefined();
			expect(player.sprite.url).toBe('images/player.png');
		});

		it('Player should not be able to move out of bounds', function () {
			player.pos[0] = -1;
			checkPlayerBounds();

			expect(player.pos[0]).toBe(0);

			player.pos[0] = (canvas.width - player.sprite.size[0]) + 1;
			checkPlayerBounds();

			expect(player.pos[0]).toBe(canvas.width - player.sprite.size[0]);
		});
	});

	describe("Resources", function() {
		it('Should load all the required images', function () {
			expect(resources.get('images/sprites.png')).toBeDefined();
			expect(resources.get('images/invader_jb.png')).toBeDefined();
			expect(resources.get('images/explode.png')).toBeDefined();
			expect(resources.get('images/bullet.png')).toBeDefined();
			expect(resources.get('images/enemy-bullet.png')).toBeDefined();
			expect(resources.get('images/gameover.gif')).toBeDefined();
			expect(resources.get('images/player.png')).toBeDefined();
			expect(resources.get('images/starsbackground.jpg')).toBeDefined();
		});
	});

	describe("Audio", function() {
		it('Should have background music', function () {
			expect(backgroundMusic).toBeDefined();
		});

		it('Should have laser sound', function () {
			expect(laserSound).toBeDefined();
		});
	});

	describe("Invaders", function() {
		it('Should have 24 invaders', function () {
			expect(enemies.length).toBe(24);
		});

		it('Invaders should have a position', function () {
			expect(enemies[0].pos).toBeDefined();
		});

		it('Invaders should have a sprite', function () {
			expect(enemies[0].sprite).toBeDefined();
			expect(enemies[0].sprite.url).toBe('images/jb.gif');
		});

		it('They should have different positions', function () {
			expect(enemies[0].pos[0]).not.toBe(enemies[1].pos[0]);
		});

		it('They should be evenly spaced', function () {
			expect(enemies[0].pos[0] + enemies[1].pos[0]).toBe(180);
		});

		it('They should be 3 rows of invaders', function () {
			expect(enemies[enemies.length / 3].pos[1]).not.toBe(enemies[(enemies.length / 3) + 1].pos[1]);
		});
	});

	describe("Collision Detection", function() {
		it('Should detect collision when an entity collides with another entity', function () {
			expect(boxCollides(0, 30, 0, 30)).toBeTruthy();
		});
	});
});
